function wpm_insertHTML(html,field){
	var o=document.getElementById(field);
	try{
		if(o.selectionStart || o.selectionStart===0){
			o.focus();
			var os=o.selectionStart;
			var oe=o.selectionEnd;
			var np=os+html.length;
			o.value=o.value.substring(0,os)+html+o.value.substring(oe,o.value.length);
			o.setSelectionRange(np,np);
		}else if(document.selection){
			o.focus();
			sel=document.selection.createRange();
			sel.text=html;
		}else{
			o.value+=html;
		}
	}catch(e){}
}

function wpm_selectAll(chk,table,className){
	if(!className)className='check-column';
	table=document.getElementById(table);
	var th=table.getElementsByTagName('th');
	for(var i=0;i<th.length;i++){
		if(th[i].className==className && th[i].scope=='row'){
			try{
				th[i].getElementsByTagName('input')[0].checked=chk.checked;
			}catch(e){}
		}
	}
}

function wpm_delete_level(chk,level){
	var r=document.getElementById('wpm_level_row_'+level)
	var x=r.getElementsByTagName('input');
	for(var i=0;i<x.length;i++){
		if(x[i].name!='delete[]'){
			try{
				x[i].disabled=chk.checked;
			}catch(e){}
		}
	}
	var x=r.getElementsByTagName('select');
	for(var i=0;i<x.length;i++){
		try{
			x[i].disabled=chk.checked;
		}catch(e){}
	}
}

function wpm_clone_level(f){
	var srcid=f.clonefrom.value;
	var dstid=f.doclone.value;
	if(f.doclone.checked){
		f['wpm_levels['+dstid+'][isfree]'].checked=f['wpm_levels['+srcid+'][isfree]'].checked;
		f['wpm_levels['+dstid+'][role]'].selectedIndex=f['wpm_levels['+srcid+'][role]'].selectedIndex;
		f['wpm_levels['+dstid+'][loginredirect]'].selectedIndex=f['wpm_levels['+srcid+'][loginredirect]'].selectedIndex;
		f['wpm_levels['+dstid+'][afterregredirect]'].selectedIndex=f['wpm_levels['+srcid+'][afterregredirect]'].selectedIndex;
		f['wpm_levels['+dstid+'][allpages]'].checked=f['wpm_levels['+srcid+'][allpages]'].checked;
		f['wpm_levels['+dstid+'][allcategories]'].checked=f['wpm_levels['+srcid+'][allcategories]'].checked;
		f['wpm_levels['+dstid+'][allposts]'].checked=f['wpm_levels['+srcid+'][allposts]'].checked;
		f['wpm_levels['+dstid+'][allcomments]'].checked=f['wpm_levels['+srcid+'][allcomments]'].checked;
		f['wpm_levels['+dstid+'][expire]'].value=f['wpm_levels['+srcid+'][expire]'].value;
		f['wpm_levels['+dstid+'][calendar]'].selectedIndex=f['wpm_levels['+srcid+'][calendar]'].selectedIndex;
		f['wpm_levels['+dstid+'][expire]'].disabled=f['wpm_levels['+srcid+'][expire]'].disabled;
		f['wpm_levels['+dstid+'][calendar]'].disabled=f['wpm_levels['+srcid+'][calendar]'].disabled;
		f['wpm_levels['+dstid+'][noexpire]'].checked=f['wpm_levels['+srcid+'][noexpire]'].checked;
		f['wpm_levels['+dstid+'][disableexistinglink]'].checked=f['wpm_levels['+srcid+'][disableexistinglink]'].checked;
	}else{
		f['wpm_levels['+dstid+'][isfree]'].checked=false;
		f['wpm_levels['+dstid+'][role]'].selectedIndex=0;
		f['wpm_levels['+dstid+'][loginredirect]'].selectedIndex=0;
		f['wpm_levels['+dstid+'][afterregredirect]'].selectedIndex=0;
		f['wpm_levels['+dstid+'][allpages]'].checked=false;
		f['wpm_levels['+dstid+'][allcategories]'].checked=false;
		f['wpm_levels['+dstid+'][allposts]'].checked=false;
		f['wpm_levels['+dstid+'][allcomments]'].checked=false;
		f['wpm_levels['+dstid+'][expire]'].value='';
		f['wpm_levels['+dstid+'][calendar]'].selectedIndex=0;
		f['wpm_levels['+dstid+'][expire]'].disabled=false
		f['wpm_levels['+dstid+'][calendar]'].disabled=false;
		f['wpm_levels['+dstid+'][noexpire]'].checked=false;
		f['wpm_levels['+dstid+'][disableexistinglink]'].checked=false;
	}
}

function wpm_category_form(ischecked){
	var f=document.getElementById('editcat');
	if(!f)f=document.getElementById('addcat');
	if(!f)return;

	if(f.id=='addcat')ischecked=true;

	if(ischecked){
		var chkyes='checked="true"';
		var chkno='';
	}else{
		var chkyes='';
		var chkno='checked="true"';
	}
	var t=f.getElementsByTagName('table')[0].getElementsByTagName('tbody')[0];
	var tr=document.createElement('tr');
	tr.className='form-field';

	var th=document.createElement('th');
	th.scope='row';
	th.vAlign='top';
	th.innerHTML='Protect this Category?';
	tr.appendChild(th);

	var td=document.createElement('td');
	td.vAlign='top';
	td.innerHTML='<label><input type="radio" name="wlmember_protect_category" '+chkyes+' value="yes" /> Yes</label> &nbsp; <label><input type="radio" name="wlmember_protect_category" '+chkno+' value="no" /> No</label>';
	tr.appendChild(td);
	t.appendChild(tr);
}